<?php

use Faker\Generator as Faker;
use App\Employee;
$factory->define(Employee::class, function (Faker $faker) {
    return [
        'employee_name' => $faker->name,
        'employee_salary' => random_int(10,20) . '000',
        'employee_age' => random_int(50,100),
        'profile_image' => 'sample.png'
    ];
});
