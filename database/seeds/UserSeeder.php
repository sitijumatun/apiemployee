<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fillable = [
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('password')
        ];

        $user = new User();
        $user->fill($fillable);
        $user->save();
    }
}
