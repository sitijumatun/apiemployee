<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'API'], function () {
    Route::post('login', 'AuthController@login');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('employee', 'EmployeeController@index');
        Route::get('employee/{employee}', 'EmployeeController@show');
        Route::post('employee/create', 'EmployeeController@store');
        Route::put('employee/update/{employee}', 'EmployeeController@update');
        Route::delete('employee/delete/{employee}', 'EmployeeController@destroy');
    });
});

