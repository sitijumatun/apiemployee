<?php

namespace App\Transformers;

use App\Employee;
use League\Fractal\TransformerAbstract;

class EmployeeTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param Employee $employee
     * @return array
     */
    public function transform(Employee $employee)
    {
        return [
            'id' => $employee->id,
            'employeeName' => $employee->employee_name,
            'employeeSalary' => $employee->employee_salary,
            'employeeAge' => $employee->employee_age,
            'profileImage' => $employee->profile_image,
            'profileImageUrl' => $employee->profile_image_url
        ];
    }
}
