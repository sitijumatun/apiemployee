<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;
use Carbon\Carbon;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $tokenExpired = Carbon::now()->addWeeks(1);
        $token->expires_at = $tokenExpired;
        $token->save();

        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'accessToken' => $tokenResult->accessToken,
            'tokenExpiredAt' =>  Carbon::now()->addWeeks(1)->format('Y-m-d H:i:s')
        ];
    }
}
