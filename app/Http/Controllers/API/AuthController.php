<?php

namespace App\Http\Controllers\API;

use App\Exceptions\ValidationException as FailedEntityException;
use App\Transformers\UserTransformer;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws FailedEntityException
     * @throws AuthenticationException
     */
    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|string|email',
                'password' => 'required|string'
            ]);
        } catch (ValidationException $e) {
            throw new FailedEntityException($e->getMessage());
        }

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)) {
           throw new AuthenticationException('Unauthorized!');
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Login Success!',
            'payload' => fractal($request->user(), new UserTransformer()),
        ],200);
    }
}
