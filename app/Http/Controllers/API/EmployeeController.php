<?php

namespace App\Http\Controllers\API;

use App\Employee;
use App\Exceptions\ValidationException as FailedEntityException;
use App\Transformers\EmployeeTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Facades\Image;

class EmployeeController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(){
        $employees = Employee::orderBy('employee_name')->get();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Employee Retrieved!',
            'payload' => fractal($employees, new EmployeeTransformer()),
        ],200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws FailedEntityException
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'employee_name' => 'required',
                'employee_salary' => 'required',
                'employee_age' => 'required',
            ]);
        } catch (ValidationException $e) {
            throw new FailedEntityException($e->getMessage());
        }

        $employee = new Employee();
        if($request->has('profile_image') && $request->get('profile_image') !== null) {
            $imageName = random_int(100000, 1001238912);
            $path = public_path() . '/images/' . $imageName;
            Image::make(file_get_contents($request->get('profile_image')))
                ->save($path);

            $inputData = [
                'employee_name' => $request->get('employee_name'),
                'employee_salary' => $request->get('employee_salary'),
                'employee_age' => $request->get('employee_age'),
                'profile_image' => $imageName
            ];
            $employee->fill($inputData);
        }else{
            $employee->fill($request->all());
        }
        $employee->save();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Employee Data Create Successfully!',
            'payload' => fractal($employee, new EmployeeTransformer()),
        ],200);
    }


    /**
     * @param Employee $employee
     * @return JsonResponse
     */
    public function show(Employee $employee)
    {
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Employee Data Retrieve Successfully!',
            'payload' => fractal($employee, new EmployeeTransformer()),
        ],200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Employee $employee
     * @return \Illuminate\Http\Response
     * @throws FailedEntityException
     */
    public function update(Request $request, Employee $employee)
    {
        try {
            $this->validate($request, [
                'employee_name' => 'required',
                'employee_salary' => 'required',
                'employee_age' => 'required',
            ]);
        } catch (ValidationException $e) {
            throw new FailedEntityException($e->getMessage());
        }

        if($request->has('profile_image') && $request->get('profile_image') !== null) {
            $imageName = random_int(100000, 1001238912);
            $path = public_path() . '/images/' . $imageName;
            Image::make(file_get_contents($request->get('profile_image')))
                ->save($path);

            $inputData = [
                'employee_name' => $request->get('employee_name'),
                'employee_salary' => $request->get('employee_salary'),
                'employee_age' => $request->get('employee_age'),
                'profile_image' => $imageName
            ];
            $employee->fill($inputData);
        }else{
            $employee->fill($request->all());
        }
        $employee->update();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Employee Data Update Successfully!',
            'payload' => fractal($employee, new EmployeeTransformer()),
        ],200);
    }


    /**
     * @param Employee $employee
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Employee Data Delete Successfully!',
            'payload' => [],
        ],200);
    }
}
