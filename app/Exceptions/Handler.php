<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof APIException) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'message' => $exception->getMessage(),
                'payload' => [],
            ],400);
        }

        if ($exception instanceof ValidationException) {
            return response()->json([
                'code' => 422,
                'success' => false,
                'message' => $exception->getMessage(),
                'payload' => [],
            ],422);
        }

        if($exception instanceof ModelNotFoundException) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'Data Not Found',
                'payload' => [],
            ], 404);
        }

        if($exception instanceof MethodNotAllowedHttpException) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'message' => 'Method Not Allowed!',
                'payload' => [],
            ], 400);
        }

        if($exception instanceof NotFoundHttpException) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'message' => 'Url Not Found!',
                'payload' => [],
            ], 400);
        }

        if($exception instanceof AuthenticationException ) {
            return response()->json([
                'code' => 401,
                'success' => false,
                'message' => 'Unauthorized!',
                'payload' => [],
            ], 401);
        }

        return parent::render($request, $exception);
    }
}
