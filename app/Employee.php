<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Routing\UrlGenerator;

class Employee extends Model
{
    protected $appends = ['profile_image_url'];
    protected $fillable = [
        'employee_name',
        'employee_salary',
        'employee_age',
        'profile_image'
    ];

    /**
     * @return UrlGenerator|null|string
     */
    public function getProfileImageUrlAttribute(){
        if(isset($this->attributes['profile_image'])) {
            return url(asset('images/' . $this->attributes['profile_image']));
        }

        return null;
    }
}
